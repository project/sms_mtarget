<?php

/**
 * @file
 * Module admin page.
 */

/**
 * Page callback for module admin settings form.
 */
function sms_mtarget_admin_settings_form() {
  $form = array();

  $form['sms_mtarget_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('sms_mtarget_username', ''),
  );

  $form['sms_mtarget_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('sms_mtarget_password', ''),
  );

  $form['sms_mtarget_serviceid'] = array(
    '#type' => 'textfield',
    '#title' => t('Service ID'),
    '#default_value' => variable_get('sms_mtarget_serviceid', ''),
  );

  $form['sms_mtarget_allowed_ips'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed IPs on notification callback. If left void, all IPs will be allowed.'),
    '#description' => t('Newline separated'),
    '#default_value' => variable_get('sms_mtarget_allowed_ips', ''),
  );

  return system_settings_form($form);
}

/**
 * Page callback for module test send form.
 */
function sms_mtarget_admin_send_form() {
  $form = array();

  $form['destination_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination Address'),
    '#required' => TRUE,
  );

  $form['msgtext'] = array(
    '#type' => 'textfield',
    '#title' => t('Msg Text'),
    '#size' => '140',
    '#required' => TRUE,
  );

  $form['timetosend'] = array(
    '#type' => 'textfield',
    '#title' => t('Time to send'),
  );

  $form['extended'] = array(
    '#type' => 'textfield',
    '#title' => t('Extended'),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send SMS'),
  );

  $form['#submit'] = array('sms_mtarget_admin_send_form_submit');

  return $form;
}

/**
 * Test send form submit callback.
 */
function sms_mtarget_admin_send_form_submit($form, $form_state) {
  $timetosend = empty($form_state['values']['timetosend']) ? NULL : $form_state['values']['timetosend'];
  $extended = empty($form_state['values']['extended']) ? NULL : $form_state['values']['extended'];
  SMSMTarget::sendText($form_state['values']['destination_address'], $form_state['values']['msgtext'], $timetosend, $extended);
}
