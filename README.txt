INTRODUCTION
------------
This module integrates the MTarget (http://www.mtarget.fr/) SMS sending 
service with Drupal. It provides an API to send messages, plus admin 
pages to set credentials.

Its submodule SMS MTarget Message provides a custom entity which is
used to log sent messages and store the result of notifications sent
by the third party service.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
  
CONFIGURATION
-------------
 * Configure MTarget credentials and allowed IPs in Administration » 
   Configuration » SMS MTarget Config.
