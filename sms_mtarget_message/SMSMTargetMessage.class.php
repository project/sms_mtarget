<?php

/**
 * @file
 * Provides an API for storing messages in Drupal.
 */

/**
 * The API class for sms_mtarget_message module.
 */
class SMSMTargetMessage {
  /**
   * Creates an new message.
   *
   * @return int
   *    The newly created message id.
   */
  public static function create($ticket, $number, $message, $code) {
    $array = array(
      'ticket' => $ticket,
      'number' => $number,
      'message' => $message,
      'code' => $code,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    $message = entity_create('sms_mtarget_message', $array);
    entity_save('sms_mtarget_message', $message);
    return $message->id;
  }

  /**
   * Returns null if there is no message.
   */
  public static function retrieveByTicket($ticket) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'sms_mtarget_message')
      ->propertyCondition('ticket', $ticket);
    $result = $query->execute();
    if (isset($result['sms_mtarget_message'])) {
      $message = array_pop($result['sms_mtarget_message']);
      $message = self::retrieve($message->id);
      return $message;
    }
    return NULL;
  }

  /**
   * Returns null if there is no message, or more than one.
   */
  public static function retrieve($id) {
    $messages = entity_load('sms_mtarget_message', array($id));

    if (count($messages) == 1) {
      return $messages[$id];
    }
    return NULL;
  }

  /**
   * Updates message entity. Fails if it doesn't exist.
   */
  public static function update($message) {
    $message->changed = time();
    entity_save('sms_mtarget_message', $message);
  }

  /**
   * Deletes message instance. Fails if it doesn't exist.
   */
  public static function delete($id) {
    entity_delete('sms_mtarget_message', $id);
  }

}
