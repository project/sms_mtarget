<?php

/**
 * @file
 * SMS MTarget Message Views Controller class.
 */

/**
 * SMS MTarget message entity controller for generating views integration.
 */
class SMSMTargetMessageViewsController extends EntityDefaultViewsController {
  /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    $data['sms_mtarget_message']['created']['field']['handler'] = 'views_handler_field_date';
    $data['sms_mtarget_message']['changed']['field']['handler'] = 'views_handler_field_date';
    return $data;
  }

}
