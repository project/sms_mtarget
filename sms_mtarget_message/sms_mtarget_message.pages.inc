<?php

/**
 * @file
 * MTarget module pages.
 */

/**
 * Page callback for notication menu callback.
 */
function _sms_mtarget_notification() {
  $message = SMSMTargetMessage::retrieveByTicket($_POST['MsgId']);
  if ($message) {
    if (isset($_POST['Status']) && is_numeric($_POST['Status'])) {
      $message->status = $_POST['Status'];
      SMSMTargetMessage::update($message);
    }
  }

  drupal_exit();
}
