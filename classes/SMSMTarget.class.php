<?php

/**
 * @file
 * MTarget API class.
 */

/**
 * The API class for sms_mtarget module.
 */
class SMSMTarget {
  /**
   * This method sends the message.
   * It guesses which MTarget method to use based on passed params.
   *
   * @param string $number
   *    The international style formatted number (ex: +33123456789).
   * @param string $message
   *    The message to send.
   * @param string $timetosend
   *    See MTarget documentation.
   * @param string $extended
   *    See MTarget documentation.
   * @param bool $enqueue
   *    Wether to send the message synchronously, or to add it to the queue.
   */
  public static function sendText($number, $message, $timetosend = NULL, $extended = NULL, $enqueue = FALSE) {

    if (strlen($number) != 12) {
      watchdog('sms_mtarget', 'Phone number %number is invalid.', array('%number' => $number), WATCHDOG_WARNING);
      return;
    }

    if ($enqueue) {
      $data = array();
      $data['number'] = $number;
      $data['message'] = $message;
      $data['timetosend'] = $timetosend;
      $data['extended'] = $extended;

      $queue = DrupalQueue::get('sms_mtarget_outgoing_messages');
      $queue->createItem($data);
    }
    else {
      $url = 'http://smswebservices.mtarget.fr/SmsWebServices/ServletSms';

      $query = array(
        'username' => variable_get('sms_mtarget_username', ''),
        'password' => variable_get('sms_mtarget_password', ''),
        'serviceid' => variable_get('sms_mtarget_serviceid', ''),
        'encoding' => 'UTF-8',
        'destinationAddress' => $number,
        'originatingAddress' => '00000',
        'operatorid' => '0',
        'paycode' => '0',
        'msgtext' => $message,
      );

      if ($timetosend == NULL && $extended == NULL) {
        $query['method'] = 'sendText';
      }
      elseif ($timetosend != NULL && $extended == NULL) {
        $query['method'] = 'sendTextDiffered';
      }
      elseif ($timetosend == NULL && $extended != NULL) {
        $query['method'] = 'sendTextExtended';
      }
      elseif ($timetosend != NULL && $extended != NULL) {
        $query['method'] = 'sendTextDifferedExtended';
      }

      if ($timetosend != NULL) {
        $query['timetosend'] = $timetosend;
      }
      if ($extended != NULL) {
        $query['extended'] = $extended;
      }

      $url .= '?' . drupal_http_build_query($query);

      $result = drupal_http_request($url);

      if (module_exists('sms_mtarget_message')) {
        $data = new SimpleXMLElement($result->data);
        SMSMTargetMessage::create((string) $data->TICKET, $number, $message, (int) $data->CODE);
      }
    }
  }

}
